# Christian Chown

[christianchown@gmail.com](mailto:christianchown@gmail.com) / @christianchown / [07764484446](tel:+447764484446)

Former videogames developer 🎮 turned elite JavaScript developer ⚛️

> [Click here if you want this CV as a PDF](https://www.christianchown.com/Christian_Chown_CV_2.1.0.pdf)

## Key skills

- 5 years commercial React / React Native / TypeScript / Hooks 🔥
- 15 year veteran web developer / Expert level Web APIs and CSS
- AWS / Firebase / TDD / DevOps / Agile
- Proven track record of delivering apps and websites at scale
- Redux expert, progressed onto superior state managment solutions

## React employment history

### Santander UK plc

April 2020-current

Head of Mobile Labs. Responsibilities include our business banking app and directing front end application architecture strategy, tooling and CI across our AWS estate

- React Native / ReactJS / TypeScript / xstate / Swift / Kotlin
- Github actions CI / Fastlane / App Center

### Capita plc

October 2019-March 2020

Converting a WPF application to React

- ReactJS / TypeScript / Redux Saga / SASS / web workers

### Santander UK plc

February 2019-September 2019

Contracted to create a greenfield React Native banking app for corporate users

- React Native / TypeScript / Objective C / Swift / Java / JNI
- Redux / Jest / Arxan / Trusteer / White box cryptography

### Double Brace ltd

September 2018-February 2019

Greenfield React development, developing client-side insurance portal

- React / TypeScript / Redux / Formik / Jest

### Careplanner ltd

March 2018-December 2019

Rewriting and updating legacy Titanium app to React Native

- React Native / ES6 / Redux Saga / RealmJS / Jest / Crashlytics

[![Screenshots of CarePlanner React Native app](https://www.christianchown.com/images/careplanner-react-native.png "Screenshots of CarePlanner React Native app")](https://play.google.com/store/apps/details?id=uk.co.careplanner2)

- [Android - CarePlanner in the Play Store](https://play.google.com/store/apps/details?id=uk.co.careplanner2)
- [iOS - CarePlanner in the App Store](https://itunes.apple.com/gb/app/careplanner-2/id895779361)

### Parentscope Limited

August 2016-current

Director and tech lead. Developed entire technical infrastructure: 2 apps from zero code to app and play stores, 4 websites, 3 node workers (to date)

- React Native / React / NodeJS / Typescript / Redux / Firebase (Realtime database (NoSQL), Authentication, Cloud Messaging, Storage)
- Express / Winston / Jest / Enzyme / CodePush / Express proxied through nginx / SSR / SASS / BEM

[![Screenshots of Parentscope(UK) React Native app](https://www.christianchown.com/images/parentscope-react-native.png "Screenshots of Parentscope(UK) React Native app")](https://play.google.com/store/apps/details?id=com.parentscope)

[![Screenshots of Parentscope React websites](https://www.christianchown.com/images/parentscope-react.png "Screenshots of Parentscope React websites")](https://www.parentscope.co.uk)

- [Android - Parentscope(UK) in the Play Store](https://play.google.com/store/apps/details?id=com.parentscope)
- [iOS - Parentscope(UK) in the App Store](https://itunes.apple.com/us/app/parentscope-uk/id1251075347)

Other workflow: GIT, Yarn, TS & ESLint, Grunt, Sketch, Slack

## Employment history

My career can be split into two phases. After university, I became a videogames developer before transitioning to the web.

### Current career phase - Web developer 🌐

#### Evosite (April 2012 - April 2018)

https://www.evosite.co.uk
Head of development

Developed Evosite's bespoke OOP PHP framework, currently powering 100+ sites, with the ecommerce platform processing £20m+ in transactions per year.

In the time with Evosite I helped the company evolve from procedural PHP developed directly on the server, to a modern, best-practise development agency. Along with development, my responsibilities included

- Mentoring of junior developers
- Performing code reviews
- Producing technical specifications and analysis
- Developing QA, documentation, deployment and other technical processes

References:

- James Shakespeare - CEO - [james@evosite.co.uk](mailto:james@evosite.co.uk) - [01823 278500](tel:+441823278500)
- Guy Tucker - Managing Director - [guy@evosite.co.uk](mailto:guy@evosite.co.uk) - [01823 278500](tel:+441823278500)

[![Screenshots of Evosite websites](https://www.christianchown.com/images/evosite-websites.png "Screenshots of Evosite websites")](https://www.evosite.co.uk)

#### Freelance developer (2005 - April 2012)

Prior to moving to Somerset and working with Evosite on a permanent basis, I worked as a contract and freelance web developer, primarily working in:

- PHP / C# .Net / Actionscript 3

My primary clients included:

- Net-workers - http://www.net-workers.co.uk - _Reference_ Jane Oldroyd - [jane@net-workers.org](mailto:jane@net-workers.org)
- Numiko - http://numiko.com - _Reference_ Dave Eccles - [dave@numiko.com](mailto:dave@numiko.com)
- Brass - https://www.brassagency.com - _Reference_ Paul Hetherington - [p.hetherington@brassagency.com](mailto:p.hetherington@brassagency.com)
- Branded3 - https://www.branded3.com - _Reference_ Vin Chinnaraja - [vin@branded3.com](mailto:vin@branded3.com)

Clients worked for include:

- Unilever - .NET - developed their UK internal packaging approval system
- Ribena - Flash/AS3 - developed an MP3 beat-matched animated game
- Texecom - PHP - website & CMS
- Leeds College - PHP - website & CMS
- Eucanlearn - PHP - website, CMS, online capability assessment system
- Maginus - .NET - website & CMS

## Other development skills & work

Legacy skillset

- LEMP
- LAMP
- OOP
- Bootstrap
- MySQL / MSSQL / PostgresSQL / Caché SQL
- jQuery
- AngularJS
- LESS
- Photoshop
- Fireworks
- PhantomJS
- Actionscript
- C# .NET
- Apache
- IIS
- Wordpress
- Magento
- C++
- C

Integrations written for

- Sage 200
- eBay
- SAP
- SagePay
- CardSave
- CardStream
- SoEasyPay
- Realex
- Paypal (Basic, Express & Pro)
- Dynamics CRM
- Clickdimensions
- Mailchimp
- Campaign Monitor
- Dotmailer
- Facebook
- Twitter
- Youtube
- Google Maps
- UPS
- PostcodeAnywhere
- Google Analytics
- prof.it
- idibu
- Wonde

and numerous one-offs.

Services:

- AWS (Lambda, Amplify, S3, SES, EKS, EC2, Elasticsearch, Route 53, API Gateway)
- Firebase (realtime database, authentication, cloud messaging, cloud storage, analytics)
- Redis
- Memcache
- Elasticsearch

(and probably a lot of others that I have forgotten about)

## Initial career phase - Videogame developer 🎮

Most of my time in the videogames industry was spent in developing 3D rendering engines for each platform as they appeared; mostly, because I could do the maths.

> Videogames claim to fame!
>
> _I wrote one of the sequels to Lemmings._
>
> [![Oh no!](https://www.christianchown.com/images/lemming.png "Oh no!")](https://en.wikipedia.org/wiki/Lemmings_Revolution)

### R8 Games (2003-2005)

- Founder, developing the title _Criminalympics_ for PS2. Also contributed to the X-Men title _X2: Wolverine's Revenge_ for PS2

### Psygnosis/Sony Computer Entertainment (1998-2003)

- Programmer, later Lead Programmer, developing racing title _Stunt Dudes_ for PS1, _Lemmings Revolution_ for PC, and the 3D action adventure title _Rogue_ for PS2

### Prometheus Entertainment (1997-1998)

- Programmer, developing _HMS Carnage_ for PC and PS1

### Ocean Software (1994-1997)

- Programmer, working on 3D engines for PC and PS1.

## Education

### University - UMIST

- BSc(Hons) in Computation (2:1)

### College - Somerset College

- A level Computer Science (grade A)
- A level Pure Maths (grade A)
- A level Statistics (grade A)

### School - Heathfield Community School

- 2 O Levels - Mathematics and English
- 8 GCSEs - grades A-C

## Technical articles

- [Simple routing in React Native with _React Native Easy Router_](https://medium.com/@christianchown/simple-routing-in-react-native-with-react-native-easy-router-6e529866519e)
- [Reading Appcelerator Titanium app properties in React Native](https://medium.com/@christianchown/reading-appcelerator-titanium-app-properties-in-react-native-47a9b016c2f4)
- [Treat your Firebase Database like a giant Redux Store](https://medium.com/@christianchown/treat-your-firebase-database-like-a-giant-redux-store-f0dfc9e46acc)
- [Putting the FERN into development](https://medium.com/@christianchown/bringing-the-fern-to-development-5d8c0d561e1c)
- [If English is your first language, you’re probably doing BEM wrong](https://medium.com/@christianchown/if-english-is-your-first-language-youre-probably-doing-bem-wrong-7caa6f9bad9b)

## Links

Some sites I've worked on

- [https://www.parentscope.co.uk](https://www.parentscope.co.uk)
- [https://www.flogas.co.uk](https://www.flogas.co.uk)
- [https://www.johnpacker.co.uk](https://www.johnpacker.co.uk)
- [https://www.olivettiagency.uk](https://www.olivettiagency.uk)
- [https://www.mkidsgolf.com](https://www.mkidsgolf.com)

Some games I worked on

- [X2: Wolverine's Revenge](https://en.wikipedia.org/wiki/X2:_Wolverine's_Revenge)
- [Lemmings Revolution](https://en.wikipedia.org/wiki/Lemmings_Revolution)

Some companies I've worked for

- [Evosite](https://www.evosite.co.uk)
- [Net-workers](http://www.net-workers.co.uk)
- [Sony Computer Entertainment Europe](https://www.sie.com/en/index.html)
- [Ocean Software](https://en.wikipedia.org/wiki/Ocean_Software)

## Q & A

- _Why did you become a developer?_

I must have been 8 or 9, and had never touched a computer when, in a department store, my elder brother walked up to a Vic 20 in the fledgling computer department and typed:

```
10 PRINT "CHRIS IS A WALLY"
20 GOTO 10
```

There, on the screen, repeated over and over, was my brother's insult, writ large.

My initial feeling wasn't to feel aggrieved, or to punch him, but _wow_. Here was a screen - something I'd only ever watched television on - and yet my brother had changed its display _purely as a matter of will_.

I knew straight away that these devices were wondrous. Using nothing more than your wits you could create _anything_ on them, limited only by your imagination.

I still punched him later, though.

- _Why did you become a videogames developer?_

For the same reason I went to UMIST rather than any other university. UMIST wanted higher grades and touted itself as being the hardest course I could find. The videogames industry offered the biggest challenge, the most difficult work and wanted the best people.

- _Why switch to the web?_

When I started at Ocean, a small team of 6 or 7 could create something truly world-beating in under 12 months (in fact, just before I joined, Ocean had 2 of the top 10 games in the world in Robocop 3 and Batman: The Movie). As the industry grew, the team sizes, development costs and risks ballooned to the detriment of the the impact and creativity a person could have working in them.

By the time I left Psygnosis, the only way you could create an original and world-beating product was with a team of 60-70 people, a budget over £10m, and 3 years. Now, of course, AAA titles have teams in the hundreds spending sums that dwarf the costs of Hollywood movies, with all the institutional inertia and risk aversion that comes with handling those mammoth budgets.

When I switched to the web, I found that once again, a small team of focused, skilled and committed individuals could do some serious damage and create wonderful and innovative products.

- _Can I ask you a question?_

Sure, just [drop me an email to christianchown@gmail.com](mailto:christianchown@gmail.com), or [raise an open issue in this repo](https://gitlab.com/christianchown/cv/issues)
